package pl.sda;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.implcitpoly.EternalStudent;
import pl.sda.implcitpoly.GraduatedStudent;
import pl.sda.implcitpoly.Student;
import pl.sda.joined.ContractEmployee;
import pl.sda.joined.Employee;
import pl.sda.joined.RegularEmployee;
import pl.sda.singletable.AgileProject;
import pl.sda.singletable.Project;
import pl.sda.singletable.StudentProject;
import pl.sda.tableperclass.BlogPost;
import pl.sda.tableperclass.Book;
import pl.sda.tableperclass.Publication;

import java.util.List;

public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();

        final Metadata metadata = new MetadataSources(registry)
                .buildMetadata();

        SessionFactory sf = metadata.buildSessionFactory();
        LOGGER.info("APP UP!");
        setupProjects(sf);
        setupPublications(sf);
        setupEmployee(sf);
        setupStudents(sf);
        sf.close();
    }

    static void setupProjects(SessionFactory sessionFactory) {
        LOGGER.info("Setting projects with single table strategy");
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        AgileProject agile = new AgileProject();
        agile.setDescription("agile description");
        agile.setGoal("finish before deadline");
        agile.setNumberOfWeeks(2);
        session.save(agile);

        StudentProject studentProject = new StudentProject();
        studentProject.setDescription("student project");
        studentProject.setScore(5);
        session.save(studentProject);

        List<Project> project = session.createQuery("FROM Project", Project.class).list();
        LOGGER.info("Query From Project: {}", project.size());

        session.getTransaction().commit();
        session.close();
    }

    static void setupPublications(SessionFactory sessionFactory) {
        LOGGER.info("Setting publication with table per class strategy");
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        Publication publication = new Publication();
        publication.setTitle("some publication title");
        session.save(publication);

        BlogPost blogPost = new BlogPost();
        blogPost.setTitle("blog title");
        blogPost.setUrl("http://example.com");
        session.save(blogPost);

        Book book = new Book();
        book.setPages(600);
        book.setTitle("Clean code!");
        session.save(book);

        List<Publication> publications = session.createQuery("FROM Publication", Publication.class).list();
        LOGGER.info("Query From Publication: {}", publications.size());

        session.getTransaction().commit();
        session.close();
    }

    static void setupEmployee(SessionFactory sessionFactory) {
        LOGGER.info("Setting employees with joined strategy");
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        RegularEmployee regularEmployee = new RegularEmployee();
        regularEmployee.setName("regular");
        regularEmployee.setHoliday(26);
        session.save(regularEmployee);

        ContractEmployee contractEmployee = new ContractEmployee();
        contractEmployee.setName("contract");
        contractEmployee.setContractType("B2B");
        session.save(contractEmployee);

        List<Employee> employees = session.createQuery("FROM Employee", Employee.class).list();
        LOGGER.info("Query From Employee: {}", employees.size());

        session.getTransaction().commit();
        session.close();
    }

    static void setupStudents(SessionFactory sessionFactory) {
        LOGGER.info("Setting students");
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        EternalStudent eternalStudent = new EternalStudent();
        eternalStudent.setName("eternal");
        eternalStudent.setUniversity("SDA");
        session.save(eternalStudent);

        GraduatedStudent graduatedStudent = new GraduatedStudent();
        graduatedStudent.setName("grad");
        graduatedStudent.setUniversity("SDA");
        session.save(graduatedStudent);

        List<Student> students = session.createQuery("FROM pl.sda.implcitpoly.Student", Student.class).list();
        LOGGER.info("Query From Student: {}", students.size());

        session.getTransaction().commit();
        session.close();
    }
}
