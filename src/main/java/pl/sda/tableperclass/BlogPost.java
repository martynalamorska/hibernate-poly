package pl.sda.tableperclass;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity
public class BlogPost extends Publication {
    @Column
    private String url;
}
