package pl.sda.singletable;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("agile_project")
public class AgileProject extends Project {

    @Column
    private String goal;

    @Column
    private Integer numberOfWeeks;
}
