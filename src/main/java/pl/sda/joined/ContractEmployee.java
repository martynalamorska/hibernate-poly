package pl.sda.joined;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@NoArgsConstructor
@Data
@Entity
public class ContractEmployee extends Employee {
    @Column
    private String contractType;
}
