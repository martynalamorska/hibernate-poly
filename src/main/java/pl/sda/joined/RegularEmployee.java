package pl.sda.joined;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;

@NoArgsConstructor
@Data
@Entity
@PrimaryKeyJoinColumn
public class RegularEmployee extends Employee {
    @Column
    private Integer holiday;
}
