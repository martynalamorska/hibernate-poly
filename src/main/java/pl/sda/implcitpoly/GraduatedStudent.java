package pl.sda.implcitpoly;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Polymorphism;
import org.hibernate.annotations.PolymorphismType;

import javax.persistence.*;

@Setter
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table
@Polymorphism(type = PolymorphismType.IMPLICIT)
public class GraduatedStudent implements Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private String university;

    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getUniversity() {
        return this.university;
    }
}
