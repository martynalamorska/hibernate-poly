package pl.sda.implcitpoly;

public interface Student {
    Integer getId();
    String getName();
    String getUniversity();
}
